vershini = [0, 1, 2, 3, 4, 5]
rebra = {(0, 1): 1, (0, 2): 1.5, (0, 3): 2, (1, 3): 0.5,
         (1, 4): 2.5, (2, 3): 1.5, (3, 5): 1, }

# данные о графе в формате ((начальная вершина, конечная): длина ребра)


def auf(vershini, rebra, start_vershina=0):
    path_lengths = {v: float('inf') for v in vershini} # временное обнуление значений пути до вершин
    path_lengths[start_vershina] = 0 # обнуление стартовой точки

    sosedi = {v: {} for v in vershini} # словарь в формате вершина: пустое множество
    for (u, v), w_uv in rebra.items(): # инициализация путей
        # u - начальная вершина, а v - конечная вершина, w_uv - расстояние между вершинами
        sosedi[u][v] = w_uv
        sosedi[v][u] = w_uv

    temporary = [v for v in vershini]

    while len(temporary) > 0: # цикл составления путей до удаления последней вершины
        granitsi = {v: path_lengths[v] for v in temporary} # составление из данных графа путей
        u = min(granitsi, key=granitsi.get) # выбор близлежащей вершины с мин расстоянием от заданной

        temporary.remove(u) # удаление вершины во избежании повторного ее прохождения

        for v, w_uv in sosedi[u].items():
            path_lengths[v] = min(path_lengths[v], path_lengths[u] + w_uv)

    return path_lengths


print('Кратчайшее расстояние в формате "вершина: расстояние до нее"', auf(vershini, rebra, start_vershina=0))
