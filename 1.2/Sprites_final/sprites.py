import pygame
from tkinter import *
from os import path
import random
from pygame.locals import (
    K_UP,
    K_DOWN,
    K_LEFT,
    K_RIGHT,
    K_ESCAPE,
    KEYDOWN,
    QUIT,
    RLEACCEL,
    K_SPACE
)


SCREEN_WIDTH = 1600
SCREEN_HEIGHT = 900

Objects = path.join(path.dirname(__file__), 'Objects')

# спрайты


class Player(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image = player_ship
        self.image = pygame.transform.scale(player_ship, (200, 200))
        self.rect = self.image.get_rect()
        self.radius = 65
        self.rect.centerx = SCREEN_HEIGHT / 2
        self.rect.bottom = SCREEN_HEIGHT / 2

    def update(self):
        keystate = pygame.key.get_pressed()
        if pressed_keys[K_UP]:
            self.rect.move_ip(0, -8)
        if pressed_keys[K_DOWN]:
            self.rect.move_ip(0, 8)
        if pressed_keys[K_LEFT]:
            self.rect.move_ip(-8, 0)
        if pressed_keys[K_RIGHT]:
            self.rect.move_ip(8, 0)
        if self.rect.right > SCREEN_WIDTH:
            self.rect.right = SCREEN_WIDTH
        if self.rect.left < 0:
            self.rect.left = 0
        if self.rect.top < -10:
            self.rect.top = -10
        if self.rect.bottom > 900:
            self.rect.bottom = 900

    def shoot(self):
        bullet = Bullet(self.rect.centerx, self.rect.bottom)
        all_sprites.add(bullet)
        bullets.add(bullet)


class Enemy(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.transform.scale(meteor, (100, 100))
        self.rect = self.image.get_rect()
        self.radius = 35
        self.rect.centerx = random.randint(SCREEN_WIDTH + 20, SCREEN_WIDTH + 100)
        self.rect.centery = random.randint(-10, SCREEN_HEIGHT)
        self.speedx = random.randint(5, 10)
        self.speedy = random.randint(-3, 3)

    def update(self):
        self.rect.y -= self.speedy
        self.rect.x -= self.speedx
        if self.rect.right < 0 or self.rect.top < -30 or self.rect.bottom > 930:
            self.rect.centerx = random.randint(SCREEN_WIDTH + 20, SCREEN_WIDTH + 100)
            self.rect.centery = random.randint(-10, SCREEN_HEIGHT)
            self.speedx = random.randint(4, 15)


class Bullet(pygame.sprite.Sprite):
    def __init__(self, x, y):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.transform.scale(rocket, (84, 56))
        self.rect = self.image.get_rect()
        self.rect.bottom = y
        self.rect.centerx = x
        self.speedx = 15

    def update(self):
        self.rect.x += self.speedx
        if self.rect.bottom < 0:
            self.kill()


# инициализация, создание окна
pygame.display.set_caption('Starmancer')
pygame.init()
screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
clock = pygame.time.Clock()

# картинки
background = pygame.image.load(path.join(Objects, 'back.png')).convert()
background_rect = background.get_rect()
player_ship = pygame.image.load(path.join(Objects, 'player_01.png')).convert_alpha()
meteor = pygame.image.load(path.join(Objects, 'meteor_01.png')).convert_alpha()
rocket = pygame.image.load(path.join(Objects, 'rocket_01.png')).convert_alpha()


all_sprites = pygame.sprite.Group()  # общая группа
enemies = pygame.sprite.Group()  # отдельная группа врагов для проверки столкновения
bullets = pygame.sprite.Group() # группа для снарядов
player = Player()
all_sprites.add(player)

# размножение врагов
for i in range(10):
    e = Enemy()
    enemies.add(e)
    all_sprites.add(e)

# игровой цикл
run = True
while run:
    for event in pygame.event.get():
        if event.type == KEYDOWN:
            if event.key == K_SPACE:
                player.shoot()
        elif event.type == QUIT:
            run = False
        if event.type == pygame.KEYDOWN:
            if event.key == K_ESCAPE:
                run = False
    pressed_keys = pygame.key.get_pressed()
    screen.fill((0, 0, 0))

    # обновление спрайтов

    # проверка повреждения снарядом
    list_hit_bullet = pygame.sprite.groupcollide(enemies, bullets, True, True)
    # проверяет взаимодействие спрайтов с двух групп, в данном случае удалит оба спрайта при столкновении
    for i in list_hit_bullet: # цикл на респавн противников при убийстве
        e = Enemy()
        all_sprites.add(e)
        enemies.add(e)

    # проверка повреждения игрока
    list_hit_player = pygame.sprite.spritecollide(player, enemies, False, pygame.sprite.collide_circle)
    # (спрайт_01, спрайт_02 (или группа), удалится ли объект при столкновении, коллизия для столкновения
    # (в данном случае круг, которому прописал радиус выше))
    # (можно заменить на условия с if для соотв сторон, но будет муторно)

    # ткинтер
    if list_hit_player:
        run = False
        root = Tk()

        def game_over():
            root.destroy()

        root.geometry('800x600')
        root.resizable(width=False, height=False)

        canva = Canvas(root, height=800, width=600)
        canva.pack()

        title = Label(canva, text='Поставить 5 баллов за игру', font=600)
        title.pack()
        btn_01 = Button(canva, text='Ладно.', font=600, command=game_over)
        btn_01.pack()

        root.mainloop()

    screen.blit(background, background_rect)

    # обновление спрайтов
    all_sprites.update()

    all_sprites.draw(screen)
    pygame.display.flip()
    clock.tick(60)

pygame.quit()
