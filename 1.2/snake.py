import pygame
import sys
import random
from pygame.math import Vector2

pygame.init()

cells_size = 40
cells_number = 20

class DOOMGUY():
    def __init__(self):
        self.body = [Vector2(7, 10), Vector2(6, 10), Vector2(5, 10)]
        self.direction = Vector2(1, 0)
        self.new_block = False

    def draw_guy(self):
        for block in self.body:
            block_x = int(block.x * cells_size)
            block_y = int(block.y * cells_size)
            block_rect = pygame.Rect(block_x, block_y, cells_size, cells_size)
            pygame.draw.rect(screen, pygame.Color('black'), block_rect)

    def move_guy(self):
        if self.new_block == True:
            body_copy = self.body[:]
            body_copy.insert(0, body_copy[0] + self.direction)
            self.body = body_copy[:]
            self.new_block = False
        else:
            body_copy = self.body[:-1]
            body_copy.insert(0, body_copy[0] + self.direction)
            self.body = body_copy[:]

    def add_blood(self):
        self.new_block = True


class DEMON():
    def __init__(self):
        self.random_demon()

    def draw_demon(self):
        demon_rect = pygame.Rect(int(self.pos.x * cells_size), int(self.pos.y * cells_size), cells_size, cells_size)
        pygame.draw.rect(screen, pygame.Color('green'), demon_rect)

    def random_demon(self):
        self.x = random.randint(0, cells_number - 1)
        self.y = random.randint(0, cells_number - 1)
        self.pos = pygame.math.Vector2(self.x, self.y)


class MAIN():
    def __init__(self):
        self.doomguy = DOOMGUY()
        self.demon = DEMON()

    def update(self):
        self.doomguy.move_guy()
        self.check_collision()
        self.check_fail()

    def draw_elements(self):
        self.demon.draw_demon()
        self.doomguy.draw_guy()

    def check_collision(self):
        if self.demon.pos == self.doomguy.body[0]:
            self.demon.random_demon()
            self.doomguy.add_blood()
            print('Snack')

    def check_fail(self):
        if (self.doomguy.body[0].x < 0) or (self.doomguy.body[0].x >= cells_number) or (self.doomguy.body[0].y < 0) or \
                (self.doomguy.body[0].y >= cells_number):
            self.game_over()

        for block in self.doomguy.body[1:]:
            if block == self.doomguy.body[0]:
                self.game_over()

    def game_over(self):
        pygame.quit()
        sys.exit()


screen = pygame.display.set_mode((cells_number * cells_size, cells_number * cells_size))
clock = pygame.time.Clock()

SCREEN_UPDATE = pygame.USEREVENT
pygame.time.set_timer(SCREEN_UPDATE, 150)

main_game = MAIN()
while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()
        if event.type == SCREEN_UPDATE:
            main_game.update()
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_UP:
                if main_game.doomguy.direction.y != 1:
                    main_game.doomguy.direction = Vector2(0, -1)
            if event.key == pygame.K_DOWN:
                if main_game.doomguy.direction.y != -1:
                    main_game.doomguy.direction = Vector2(0, 1)
            if event.key == pygame.K_RIGHT:
                if main_game.doomguy.direction.x != -1:
                    main_game.doomguy.direction = Vector2(1, 0)
            if event.key == pygame.K_LEFT:
                if main_game.doomguy.direction.x != 1:
                    main_game.doomguy.direction = Vector2(-1, 0)
    screen.fill((175, 215, 70))
    main_game.draw_elements()
    pygame.display.update()
    clock.tick(60)
