import pygame
import os

pygame.init()
WIN = pygame.display.set_mode([800, 800])
pygame.display.set_caption('Tree Clicker')

SUMMER_TREE_01_IMAGE = pygame.image.load(os.path.join('Assets', 'summertree_01.png'))
SUMMER_TREE_01 = pygame.transform.scale(SUMMER_TREE_01_IMAGE, (200, 200))

AUTUMN_TREE_01_IMAGE = pygame.image.load(os.path.join('Assets', 'autumntree_01.png'))
AUTUMN_TREE_01 = pygame.transform.scale(AUTUMN_TREE_01_IMAGE, (200, 200))

SUMMER_TREE_02_IMAGE = pygame.image.load(os.path.join('Assets', 'summertree_02.png'))
SUMMER_TREE_02 = pygame.transform.scale(SUMMER_TREE_02_IMAGE, (200, 200))

AUTUMN_TREE_02_IMAGE = pygame.image.load(os.path.join('Assets', 'autumntree_02.png'))
AUTUMN_TREE_02 = pygame.transform.scale(AUTUMN_TREE_02_IMAGE, (200, 200))

SUMMER_TREE_03_IMAGE = pygame.image.load(os.path.join('Assets', 'summertree_03.png'))
SUMMER_TREE_03 = pygame.transform.scale(SUMMER_TREE_03_IMAGE, (200, 200))

AUTUMN_TREE_03_IMAGE = pygame.image.load(os.path.join('Assets', 'autumntree_03.png'))
AUTUMN_TREE_03 = pygame.transform.scale(AUTUMN_TREE_03_IMAGE, (200, 200))

FPS = 1

def draw_window():
    WIN.fill((242, 242, 240))
    WIN.blit(SUMMER_TREE_01, (300, 400))
    WIN.blit(SUMMER_TREE_02, (100, 400))
    WIN.blit(SUMMER_TREE_03, (500, 400))
    pygame.display.update()

def main():
    clock = pygame.time.Clock()
    d = 0
    run = True
    while run:
        clock.tick(FPS)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False
            draw_window()
            if event.type == pygame.MOUSEBUTTONDOWN:
                mx, my = pygame.mouse.get_pos()
                print(mx, my)
                if ((mx in range(341, 449)) and (my in range (423, 574))) or ((mx in range(152, 239)) and (my in range (475, 571))) or ((mx in range(569, 632)) and (my in range (426, 569))):
                    d = 1
                    #WIN.blit(AUTUMN_TREE_01, (100, 400))
                    #pygame.display.update()
        if d == 1:
            WIN.blit(AUTUMN_TREE_01, (300, 400))
            WIN.blit(AUTUMN_TREE_02, (100, 400))
            WIN.blit(AUTUMN_TREE_03, (500, 400))
            pygame.display.update()


    pygame.quit()

main()
