import math

f = 0
while f == 0:
    try:
        class Hex():

            def __init__(self, x):
                self.__x = x
                self.calc_area()

            def set_x(self, new_x):
                self.__x = new_x
                self.calc_area()

            def calc_area(self):
                self.__area = (((self.__x ** 2) * math.sqrt(3)) / 4) * 6

            def x(self):
                return self.__x

            def area(self):
                return self.__area


        print("Введите радиус описанной окружности ")

        r = Hex(int(input()))

        print("radius: ", r.x())

        print("area: ", r.area())

        f = 1

    except:
        print("Введите корректное значение")
