from tkinter import *
from tkinter import ttk
import math
#from tkinter.ttk import Combobox
from tkinter.ttk import Radiobutton
file = open('data/tk_task.txt', 'r', encoding='utf-8')
line = file.readlines()
p = []

def next0():
    tab_control.select(tab1)
def next1():
    global p
    tab_control.select(tab2)
    #combo.get()
    print(type(selected.get()))
    if selected.get() == 1:
        p.append(1)
        print(p)
def next2():
    tab_control.select(tab3)
    print(type(selected.get()))
    if selected.get() == 2:
        p.append(1)
        print(p)
def next3():
    tab_control.select(tab4)
    print(type(selected.get()))
    if selected.get() == 1:
        p.append(1)
        print(p)
def next4():
    tab_control.select(tab5)
    print(type(selected.get()))
    if selected.get() == 3:
        p.append(1)
        print(p)
def next5():
    tab_control.select(tab6)
    print(type(selected.get()))
    if selected.get() == 2:
        p.append(1)
        print(p)
def result():
    print(sum(p))
    marks["text"] = sum(p)
    percent["text"] = math.ceil(sum(p)*100/5)

window = Tk()

window.title("Quick Quiz")
window.geometry('800x600')
tab_control = ttk.Notebook(window)

tab0 = ttk.Frame(tab_control)
tab1 = ttk.Frame(tab_control)
tab2 = ttk.Frame(tab_control)
tab3 = ttk.Frame(tab_control)
tab4 = ttk.Frame(tab_control)
tab5 = ttk.Frame(tab_control)
tab6 = ttk.Frame(tab_control)
tab_control.add(tab0, text='Начать Квиз')
tab_control.add(tab1, text='Первый')
tab_control.add(tab2, text='Второй')
tab_control.add(tab3, text='Третий')
tab_control.add(tab4, text='Четвертый')
tab_control.add(tab5, text='Пятый')
tab_control.add(tab6, text='Результаты')
selected = IntVar()

#--------------Start---------------------

but0 = Button(tab0, text='Начать Квиз!', command=next0)
but0.place(y=190, x=400)

#----------------------------------------
#--------------lbl1----------------------

lbl1 = Label(tab1, text='Вопрос 1')
lbl1.place(y=10, x=400)
but1 = Button(tab1, text='Ответить', command=next1)
but1.place(y=190, x=400)
question = Label(tab1, text=line[0])
question.place(y=100, x=360)
rad1 = Radiobutton(tab1, text='10', value=1, variable=selected)
rad2 = Radiobutton(tab1, text='15', value=2, variable=selected)
rad3 = Radiobutton(tab1, text='9', value=3, variable=selected)
rad1.place(y=230, x=360)
rad2.place(y=230, x=420)
rad3.place(y=230, x=480)

#----------------------------------------
#--------------lbl2----------------------

lbl2 = Label(tab2, text='Вопрос 2')
lbl2.place(y=10, x=400)
but1 = Button(tab2, text='Ответить', command=next2)
but1.place(y=190, x=400)
question = Label(tab2, text=line[1])
question.place(y=100, x=350)
rad1 = Radiobutton(tab2, text='1999', value=1, variable=selected)
rad2 = Radiobutton(tab2, text='2002', value=2, variable=selected)
rad3 = Radiobutton(tab2, text='2003', value=3, variable=selected)
rad1.place(y=230, x=360)
rad2.place(y=230, x=420)
rad3.place(y=230, x=480)

#----------------------------------------
#--------------lbl3----------------------

lbl3 = Label(tab3, text='Вопрос 3')
lbl3.place(y=10, x=400)
but1 = Button(tab3, text='Ответить', command=next3)
but1.place(y=190, x=400)
question = Label(tab3, text=line[2])
question.place(y=100, x=350)
rad1 = Radiobutton(tab3, text='Бегущий по лезвию', value=1, variable=selected)
rad2 = Radiobutton(tab3, text='Звездные войны', value=2, variable=selected)
rad3 = Radiobutton(tab3, text='Арда', value=3, variable=selected)
rad1.place(y=230, x=250)
rad2.place(y=230, x=400)
rad3.place(y=230, x=520)

#----------------------------------------
#--------------lbl4----------------------

lbl3 = Label(tab4, text='Вопрос 4')
lbl3.place(y=10, x=400)
but1 = Button(tab4, text='Ответить', command=next4)
but1.place(y=190, x=400)
question = Label(tab4, text=line[3])
question.place(y=100, x=350)
rad1 = Radiobutton(tab4, text='зло', value=1, variable=selected)
rad2 = Radiobutton(tab4, text='игра Маркуса Перссона', value=2, variable=selected)
rad3 = Radiobutton(tab4, text='моя жизнь', value=3, variable=selected)
rad1.place(y=230, x=300)
rad2.place(y=230, x=360)
rad3.place(y=230, x=520)

#----------------------------------------
#--------------lbl5----------------------

lbl5 = Label(tab5, text='Вопрос 5')
lbl5.place(y=10, x=400)
but1 = Button(tab5, text='Ответить', command=next5)
but1.place(y=190, x=400)
question = Label(tab5, text=line[4])
question.place(y=100, x=300)
rad1 = Radiobutton(tab5, text='перепроходил серию Deus Ex', value=1, variable=selected)
rad2 = Radiobutton(tab5, text='смотрел мемы', value=2, variable=selected)
rad3 = Radiobutton(tab5, text='баловался пивом', value=3, variable=selected)
rad1.place(y=230, x=200)
rad2.place(y=230, x=400)
rad3.place(y=230, x=520)

#----------------------------------------
#--------------result--------------------


marks = Label(tab6, text="Здесь будет ваше количество правильных ответов")
marks.place(y=100, x=300)
percent = Label(tab6, text="Здесь будет ваш процент правильных ответов")
percent.place(y=140, x=300)
but1 = Button(tab6, text='Узнать', command=result)
but1.place(y=190, x=400)


#----------------------------------------
tab_control.pack(expand=1, fill='both')
window.mainloop()
