def dl_01(ex):
    return len(str(ex))


def dl_02(ed):
    if ed % 2 == 0:
        return ed


def dl_03(er):
    if len(str(er)) != 0:
        return er


f_01 = ['Я', 'Ем', 'Банан']
f_02 = ['Банан', 'Я', 'Ем']
f_03 = ['Ем', 'Банан', 'Я']
g_01 = [1, 6, 9, 13]
g_02 = [7, 3, 10, 26]

a = map(lambda x: x * 2, [2, 6, 10, 21, 7])
print(list(a))
b = map(lambda x, y, z: x * y * z, [4, 7, 11, 23, 17], [3, 8, 40, 19, 5], [1, 5, 10.5, 2.1, 3.4])
print(list(b))
c = map(dl_01, [234, 117000, 256300, 203, 9])
print(list(c))
d = filter(dl_02, [1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
print(list(d))
e = filter(dl_03, [1, 'L', '', 'G', 5, 'B', '', 'T', 9, 1111])
print(list(e))
f = zip(f_01, f_02, f_03)
print(list(f))
g = zip(g_01, map(lambda x: x*2, g_02))
print(list(g))

# l = [1, 2, 3, '']
# print(any(l))
