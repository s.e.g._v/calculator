from turtle import Turtle, Screen
from math import pi, sin, cos
screen = Screen()
turtle = Turtle()
screen.setworldcoordinates(-pi, -5, pi, 4)
turtle.speed(0)
a = 3
parts = float(input("Количество лепестков - "))
for angle in range(1000):
    rad = a*sin(parts*angle)
    x = rad*cos(angle)
    y = rad*sin(angle)
    turtle.penup()
    turtle.goto(x, y)
    turtle.write('⚫')
screen.exitonclick()

# r = a * sin(k * φ)
# x = r * cos(φ)
# y = r * sin(φ)
