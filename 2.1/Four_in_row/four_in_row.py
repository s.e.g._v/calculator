import sys

import pygame

pygame.init()

pygame.display.set_caption('Four in row')
BLOCK_SIZE = 100
MARGIN = 10
WIDTH = BLOCK_SIZE * 6 + MARGIN * 7
HEIGHT = BLOCK_SIZE * 6 + MARGIN * 7
WINDOW = pygame.display.set_mode((WIDTH, HEIGHT))

colors = {2: (255, 220, 100), 1: (100, 120, 100), 0: (255, 255, 255)}


# --------- Вспомогательная функция транспонирования матрицы ---------------

def mtrx_trns(mtrx_c):
    s = [[mtrx_c[j][i] for j in range(len(mtrx_c))] for i in range(len(mtrx_c[0]))]
    return s


class Role:
    def __init__(self, token_color):
        self.__color = colors[int(token_color)]

    def get_token_atr(self):
        return self.__color


class Table:

    def __init__(self):
        self.start_field = [['0'] * 6 for _ in range(6)]
        self.x_new = 0
        self.y_new = 0
        self.index = 0
        self.running = True

    def update_table(self):
        x_mouse, y_mouse = pygame.mouse.get_pos()
        self.x_new = x_mouse // (BLOCK_SIZE + MARGIN)
        self.y_new = y_mouse // (BLOCK_SIZE + MARGIN)
        if self.start_field[self.x_new]:

            if self.index % 2 == 0:
                for i in range(len(self.start_field[self.x_new])):
                    if self.start_field[self.x_new][i] == '0':
                        self.start_field[self.x_new][i] = '1'
                        if i > 0:
                            self.start_field[self.x_new][i - 1] = '0'
            else:
                for i in range(len(self.start_field[self.x_new])):
                    if self.start_field[self.x_new][i] == '0':
                        self.start_field[self.x_new][i] = '2'
                        if i > 0:
                            self.start_field[self.x_new][i - 1] = '0'

        self.check_vit(self.start_field[self.x_new][self.y_new])
        self.index += 1

        return self.start_field

    def draw_table(self):
        for row in range(6):
            for column in range(6):
                color = Role(self.start_field[column][row])
                x = column * BLOCK_SIZE + (column + 1) * MARGIN
                y = row * BLOCK_SIZE + (row + 1) * MARGIN
                pygame.draw.rect(WINDOW, color.get_token_atr(), (x, y, BLOCK_SIZE, BLOCK_SIZE))

    def check_vit(self, value):

        if int(value) != 0:

            # ------- Проверка победы по колонкам --------

            for c in range(len(self.start_field)):
                for r in range(len(mtrx_trns(self.start_field)) - 3):
                    if self.start_field[r][c] == value and self.start_field[r + 1][c] == value and \
                            self.start_field[r + 2][c] == value \
                            and self.start_field[r + 3][c] == value:
                        print('Победа по строкам', value, 'игрок')
                        self.running = False

            # ------- Проверка победы по строкам ----------

            for c in range(len(self.start_field) - 3):
                for r in range(len(mtrx_trns(self.start_field))):
                    if self.start_field[r][c] == value and self.start_field[r][c + 1] == value and self.start_field[r][
                        c + 2] == value \
                            and self.start_field[r][c + 3] == value:
                        print('Победа по колонкам', value, 'игрок')
                        self.running = False

            # ------- Проверка победы по диагонали ----------

            for column in range(len(self.start_field) - 3):
                for row in range(len(mtrx_trns(self.start_field)) - 3):
                    if self.start_field[row][column] == value and self.start_field[row + 1][column + 1] == value and \
                            self.start_field[row + 2][column + 2] == value and self.start_field[row + 3][column + 3] == value:
                        print('Победа по диагонали', value, 'игрок')
                        self.running = False

            for c in range(len(self.start_field) - 3):
                for r in range(3, len(mtrx_trns(self.start_field))):
                    if self.start_field[r][c] == value and self.start_field[r - 1][c + 1] == value \
                            and self.start_field[r - 2][c + 2] == value and self.start_field[r - 3][c + 3] == value:
                        print('Победа по диагонали', value, 'игрок')
                        self.running = False


class Game:
    def __init__(self):
        self.table = Table()

    @staticmethod
    def game_loop():
        running = True
        fps = pygame.time.Clock()
        a = Table()
        Game()
        while running:
            fps.tick(200)
            WINDOW.fill((0, 0, 0))
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    running = False
                elif event.type == pygame.MOUSEBUTTONDOWN and a.running:
                    a.update_table()

            a.draw_table()
            pygame.display.update()

        pygame.quit()


v = Game()
v.game_loop()
