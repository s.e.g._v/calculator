import copy
from collections import defaultdict

file = open('plist.txt', 'r', encoding='utf-8')



torchki = []

for line in file:
    line = line.replace(' ', '')
    line = line.replace('[', ' ')
    line = line.replace(']', ' ')
    a = line.split(' ')

for i in range(1, len(a), 2):
    index = a[i].split(',')
    for j in range(len(index)):
        index[j] = float(index[j])
    torchki.append(index)


class Point:
    def __init__(self, x, y):
        self.__x_pos = x
        self.__y_pos = y

    def get_coords(self):
        return self.__x_pos, self.__y_pos


class Triangles:
    def __init__(self, point_1, point_2, point_3):
        self.__point_1 = point_1
        self.__point_2 = point_2
        self.__point_3 = point_3
        self.__area = self.area_calculate()

    def area_calculate(self):
        # ((x2 - x1)(y3 - y1) - (x3 - x1)(y2 - y1))/2
        self.__update_area = abs((self.__point_2[0] - self.__point_1[0]) * (self.__point_3[1] - self.__point_1[1])
                                 - (self.__point_3[0] - self.__point_1[0]) * (
                                         self.__point_2[1] - self.__point_1[1])) / 2
        return self.__update_area

    def __gt__(self, b):
        return self.__area > b.area

    def __lt__(self, b):
        return self.__area < b.area

    def __eq__(self, b):
        return self.__area == b.area

    @property
    def area(self):
        return self.__area


def alorithm():
    global b, a
    areas = {}
    flag = 0
    maximus = Triangles([0, 0], [0, 0], [0, 0])
    minimus = Triangles([943, 543], [986, 553], [856, 234])
    for i in range(len(torchki) - 2):
        point_01 = Point(torchki[i][0], torchki[i][1])
        for j in range(i + 1, len(torchki) - 1):
            point_02 = Point(torchki[j][0], torchki[j][1])
            for f in range(j + 1, len(torchki)):
                point_03 = Point(torchki[f][0], torchki[f][1])
                if flag == 1:
                    b = a
                a = Triangles(point_01.get_coords(), point_02.get_coords(), point_03.get_coords())

                if flag == 1:
                    if a > maximus:
                        maximus = a
                    elif (a < minimus) and (a.area != 0):
                        minimus = a

                areas[a.area] = point_01.get_coords(), point_02.get_coords(), point_03.get_coords()
                flag = 1

    print('Максимальное значение площади', maximus.area, 'Координаты - ', areas[maximus.area])
    print('Минимальное значение площади', minimus.area, 'Координаты - ', areas[minimus.area])


alorithm()
