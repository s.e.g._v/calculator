import time
import random
from tkinter import *
import tkinter
import math


def task_1():
    a = 10
    b = 2
    c = 1
    a, b, c = c, a, b
    print('a = ', a, 'b = ', b, 'c = ', c)


def task_2_1():
    flag = 0
    while flag != 2:
        try:
            first_number = int(input('Первое число - '))
            print('Да, это число')
            flag += 1
        except:
            print('Нет, это не число')

        try:
            second_number = int(input('Второе число - '))
            print('Да, это число')
            flag += 1
        except:
            print('Нет, это не число')

    print('Сумма чисел = ', first_number + second_number)


def task_2_2():
    flag = 0
    n = 4
    sum = 0
    for i in range(n):
        try:
            number = int(input('Число - '))
            print('Да, это число')
            flag = 1
            sum += number
        except:
            print('Нет, это не число')
    print('Сумма чисел = ', sum)


def task_3_1():
    number = random.randrange(0, 100, 1)
    print(number)
    print(number ** 5)


def task_3_2():
    number = random.randrange(0, 100, 1)
    print(number)
    itogo = 1
    for i in range(5):
        itogo = itogo * number

    print(itogo)


def task_4():
    n = int(input('Введите число - '))
    if n == 1:
        print('true')
    else:
        a = 1
        b = 1
        c = 0
        while c < n:
            c = a + b
            a = b
            b = c
        if c == n:
            print('Да, это число Фибоначчи')
        else:
            print('Нет, это не число Фибоначчи')


def task_5():
    mouth = input('Введите месяц - ')
    print('Ваше время года - (решено через math case) ', end='')
    match mouth:
        case 'Декабрь':
            print('Зима')
        case 'Январь':
            print('Зима')
        case 'Февраль':
            print('Зима')
        case 'Март':
            print('Весна')
        case 'Апрель':
            print('Весна')
        case 'Май':
            print('Весна')
        case 'Июнь':
            print('Лето')
        case 'Июль':
            print('Лето')
        case 'Август':
            print('Лето')
        case 'Сентбярь':
            print('Осень')
        case 'Октябрь':
            print('Осень')
        case 'Ноябрь':
            print('Осень')

    mouths = {('Декабрь', 'Январь', 'Февраль'): 'Зима',
              ('Март', 'Апрель', 'Май'): 'Весна',
              ('Июнь', 'Июль', 'Август'): 'Лето',
              ('Сентябрь', 'Октябрь', 'Ноябрь'): 'Осень'}

    for key in mouths:
        if mouth in key:
            print('Ваше время года - (решено через словарь)', mouths[key])


def task_6():
    n = int(input('Конец промежутка - '))
    sum = 0
    k_chetn = 0
    k_nechetn = 0
    for i in range(n):
        sum += i
    print(sum)
    if n % 2 == 0:
        print('Четные =', n // 2)
        print('Нечетные =', n // 2)
    else:
        print('Четные =', n // 2)
        print('Нечетные =', n // 2 + 1)


def task_7():
    n = int(input('Конец промежутка - '))

    for i in range(1, n):
        b = []
        for j in range(1, i + 1):
            if i % j == 0:
                b.append(j)
        print(i, b)


def task_8():
    general = []
    pack = []
    start = int(input('Начало промежутка - '))
    end = int(input('Конец промежутка - '))
    for a in range(start, end):
        pack = []
        for b in range(a + 1, end):
            for c in range(1, end):
                if a ** 2 + b ** 2 == c ** 2:
                    if math.gcd(a, b) == 1:
                        general.append(list((a, b, c)))

    print('Пифагоровы тройки', general)


def task_9():
    def proverka(n):
        for d in str(n):
            if d == '0' or n % int(d):
                return False
        return True

    for i in range(int(input('Начало промежутка - ')), int(input('Конец промежутка - ')) + 1):
        if proverka(i):
            print(i)


def task_10():
    n = 0
    for i in range(0, int(input('Конец промежутка - '))):
        s = 1
        for j in range(2, i // 2 + 1):
            if i % j == 0:
                s += j
        if s == i:
            n += 1
            if n < 5:
                print(i)


def task_11():
    a = [1, 2, 3, 4, 5, 6]

    print('Исходный массив', a)
    print('1 способ через a[-1] - ', a[-1])
    print('2 способ через a[len(a) - 1] - ', a[len(a) - 1])
    print('3 способ через a.pop() - ', a.pop())


def task_12():
    a = [1, 2, 3, 4, 5, 6]

    print(a[::-1])


def task_13():
    a = [1, 2, 3, 4, 5]

    def summa(b):
        if not b:
            return 0
        else:
            return b.pop(len(b) - 1) + summa(b)

    print('Массив - ', a)
    print('Сумма элементов - ', summa(a))


def task_14():
    root = Tk()
    root.title('Конвертер валюты')
    root.geometry('600x600')

    def convert_US():
        EntryR.delete(0, tkinter.END)
        EntryR.insert(0, float(EntryD.get()) * 60)

    def convert_RU():
        EntryD.delete(0, tkinter.END)
        EntryD.insert(0, float(EntryR.get()) / 60)

    # ------------------ Лейблы -------------------

    title = Label(root, text="Конвертер валюты", font='Arial 14')
    title.pack()

    dollars = Label(root, text="Сумма в долларах", font='Arial 10')
    dollars.place(y=40, x=10)

    rubles = Label(root, text="Сумма в рублях", font='Arial 10')
    rubles.place(y=40, x=420)

    # ------------------ Ввод -------------------

    EntryD = Entry(root, font='Arial 14', justify=CENTER)
    EntryD.insert(0, '0')
    EntryD.place(y=60, x=10, width=170, height=50)

    EntryR = Entry(root, font='Arial 14', justify=CENTER)
    EntryR.insert(0, '0')
    EntryR.place(y=60, x=420, width=170, height=50)

    # ------------------ Кнопки -------------------

    checks_01 = Button(root, text="Из US в RU >", command=convert_US)
    checks_01.place(y=60, x=220, width=170, height=50)

    checks_02 = Button(root, text="< из RU в US", command=convert_RU)
    checks_02.place(y=120, x=220, width=170, height=50)

    root.mainloop()


def task_15():
    def multi_table():
        for i in range(1, x + 1):
            print("")
            for j in range(1, x + 1):
                print((i * j), end='\t')
        return ""

    f = True
    while f:
        x = int(input("Введите нужную размерность от 5 до 20 "))
        if 5 <= x <= 20:
            f = False
            print('Ура')
        else:
            print('Не ура')

    print(multi_table())


def task_16():
    file = open('battlefield.txt', 'r', encoding='utf-8')

    board = []
    symbols = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J']
    board.append(symbols)

    for line in file:
        board.append(list(line.replace("\n", "")))

    def print_board(board):
        i = 0
        for row in board:
            if i < 10:
                print(i, ' |', " ".join(row))
            else:
                print(i, '|', " ".join(row))
            i += 1

    print("Поле с кораблями")
    print_board(board)


def select_task():

    actions = [task_1, task_2_1, task_2_2, task_3_1, task_3_2, task_4, task_5, task_6,
               task_7, task_8, task_9, task_10, task_11, task_12, task_13, task_14, task_15, task_16]

    flag = True

    print('Доступные операции')
    for index, item in enumerate(actions):
        print(index, item.__name__)

    while flag:

        try:
            i = int(input("Выберите задание: "))
        except ValueError:
            return print('Такого задания нету')

        if 0 <= i < len(actions):
            function = actions[i]
            function()
        else:
            flag = False


if __name__ == "__main__":
    select_task()
